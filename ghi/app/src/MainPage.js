function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">Project Beta</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          Helping you manage all aspects of your car dealership: Inventory, Sales, & Service Appointments
        </p>
      </div>
    </div>
  );
}

export default MainPage;
