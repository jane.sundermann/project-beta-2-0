import React, { useState, useEffect } from 'react';

function AppointmentForm () {
  const [vin, setVin] = useState('');
  const [customer, setCustomer] = useState('');
  const [date_time, setDateTime] = useState('');
  const [technician, setTechnician] = useState('');
  const [technicians, setTechnicians] = useState([]);
  const [reason, setReason] = useState('');

  async function fetchTechnicians() {
    const url = 'http://localhost:8080/api/technicians/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  }

  useEffect(() => {
    fetchTechnicians();
  }, [])

  async function handleSubmit(event) {
    event.preventDefault();
    const data = {
      vin,
      customer,
      date_time,
      technician,
      reason,
    };

    const appointmentUrl = 'http://localhost:8080/api/appointments/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(appointmentUrl, fetchConfig);
    if (response.ok) {
      const newAppointment = await response.json();
      setVin('');
      setCustomer('');
      setDateTime('');
      setTechnician('');
      setReason('');
    }
  }

  function handleChangeVin(event) {
    const { value } = event.target;
    setVin(value);
  }

  function handleChangeCustomer(event) {
    const { value } = event.target;
    setCustomer(value);
  }

  function handleChangeDateTime(event) {
    const { value } = event.target;
    setDateTime(value);
  }

  function handleChangeTechnician(event) {
    const { value } = event.target;
    setTechnician(value);
  }

  function handleChangeReason(event) {
    const { value } = event.target;
    setReason(value);
  }

return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a Service Appointment</h1>
          <form onSubmit={handleSubmit} id="create-appointment-form">
            <div className="form-floating mb-3">
              <input
                value={vin}
                onChange={handleChangeVin}
                placeholder="vin"
                required
                type="text"
                name="vin"
                id="vin"
                className="form-control"
              />
              <label htmlFor="name">Automobile VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={customer}
                onChange={handleChangeCustomer}
                placeholder="Customer"
                required
                type="text"
                name="customer"
                id="customer"
                className="form-control"
              />
              <label htmlFor="starts">Customer</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={date_time}
                onChange={handleChangeDateTime}
                placeholder="Date"
                required
                type="datetime-local"
                name="date_time"
                id="date_time"
                className="form-control"
              />
              <label htmlFor="starts">Date/Time</label>
            </div>
            <div className="mb-3">
              <select
                value={technician}
                onChange={handleChangeTechnician}
                required
                name="technician"
                id="technician"
                className="form-select">
                <option value="">Choose a technician</option>
                {technicians.map((technician) => {
                  return (
                    <option key={technician.id} value={technician.id}>
                      {technician.first_name} {technician.last_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input
                value={reason}
                onChange={handleChangeReason}
                placeholder="Reason"
                required
                type="text"
                name="reason"
                id="reason"
                className="form-control"
              />
              <label htmlFor="starts">Reason</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default AppointmentForm;