import React, { useEffect, useState } from "react";

function ListAllSalesPeople() {
  const [salespeople, setSalesPeople] = useState([]);

  async function fetchSalesPeople() {
    const salespeopleUrl = "http://localhost:8090/api/salespeople/";
    const response = await fetch(salespeopleUrl);

    if (response.ok) {
      const data = await response.json();
      setSalesPeople(data.sales_person);
    } else {
      alert("Error fetching salespeople data");
    }
  }

  useEffect(() => {
    fetchSalesPeople();
  }, []);

  return (
    <>
    <h1>Salespeople</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
          </tr>
        </thead>
        <tbody>
          {salespeople?.map((salesperson) => {
            return (
              <tr key={salesperson.id}>
                <td>{salesperson.employee_id}</td>
                <td>{salesperson.first_name}</td>
                <td>{salesperson.last_name}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default ListAllSalesPeople;
