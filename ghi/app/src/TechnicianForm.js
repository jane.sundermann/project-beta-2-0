import React, { useState, useEffect } from 'react';

function TechnicianForm () {
  const [first_name, setfirstName] = useState('');
  const [last_name, setlastName] = useState('');
  const [employee_id, setemployeeID] = useState('');

  useEffect(() => {
  }, [])

  async function handleSubmit(event) {
    event.preventDefault();
    const data = {
      first_name,
      last_name,
      employee_id,
    };

    const technicianUrl = 'http://localhost:8080/api/technicians/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(technicianUrl, fetchConfig);
    if (response.ok) {
      const newTechnician = await response.json();
      setfirstName('');
      setlastName('');
      setemployeeID('');
    }
  }

  function handleChangefirstName(event) {
    const { value } = event.target;
    setfirstName(value);
  }

  function handleChangelastName(event) {
    const { value } = event.target;
    setlastName(value);
  }

  function handleChangeemployeeID(event) {
    const { value } = event.target;
    setemployeeID(value);
  }

return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a new technician</h1>
          <form onSubmit={handleSubmit} id="create-technician-form">
            <div className="form-floating mb-3">
              <input
                value={first_name}
                onChange={handleChangefirstName}
                placeholder="First Name"
                required
                type="text"
                name="first_name"
                id="first_name"
                className="form-control"
              />
              <label htmlFor="name">First Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={last_name}
                onChange={handleChangelastName}
                placeholder="Last Name"
                required
                type="text"
                name="last_name"
                id="last_name"
                className="form-control"
              />
              <label htmlFor="starts">Last Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={employee_id}
                onChange={handleChangeemployeeID}
                placeholder="Employee ID"
                required
                type="text"
                name="employee_id"
                id="employee_id"
                className="form-control"
              />
              <label htmlFor="ends">Employee ID</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default TechnicianForm;
