import { useState, useEffect } from "react";

// Special Feature 1
function IsVip({vipStatus}) {
    let vip = vipStatus;
    let itemContent = "yes";
    if (!vip) {
        itemContent = "no";
    }
    return <td className="item">{itemContent}</td>;
}

function AppointmentList2() {

    const [ appointments, setAppointments ] = useState([]);

    const fetchAppointments = async() => {
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (response.ok) {
          const data = await response.json();
          setAppointments(data.appointments)
        }
      }

    useEffect(() => {
        fetchAppointments();
    }, []);

    const handleCancel = async (e) => {
        const id = e.target.value;
        const url = `http://localhost:8080/api/appointments/${id}/cancel/`;

        const data = {
            status: "Canceled"
        };

        const fetchOptions = {
            method: 'PUT',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        };
        try {
            const response = await fetch(url, fetchOptions);
            if (response.ok) {
                const canceledAppointment = await response.json();
                fetchAppointments();
            }
        } catch (err) {
            console.error(err);
        }
    }

    const handleFinish = async (e) => {
        const id = e.target.value;
        const url = `http://localhost:8080/api/appointments/${id}/finish/`;

        const data = {
            status: 'Finished'
        };

        const fetchOptions = {
            method: 'PUT',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        };
        try {
            const response = await fetch(url, fetchOptions);
            if (response.ok) {
                const finishedAppt = await response.json();
                fetchAppointments();
            }
        } catch (err) {
            console.error(err);
        }
    }

    // Transforms the date as it's displayed on the page into something more visibly pleasing than the raw data
    const formatDate = (dateString) => {
        const options = { year: 'numeric', month: 'long', day: 'numeric', hour: '2-digit', minute: '2-digit'};
        return new Date(dateString).toLocaleString('en-US', options);
    }

    return (
        <div className="appointment-list-container">
            <h1>Service Appointments</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>VIP</th>
                        <th>Customer</th>
                        <th>Date and Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map(appointment => {
                        if (appointment.status === 'created') {
                            return (
                                <tr key={appointment.id}>
                                    <td>{appointment.vin}</td>
                                    <IsVip />
                                    <td>{appointment.customer}</td>
                                    <td>{ formatDate(appointment.date_time)}</td>
                                    <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                                    <td>{appointment.reason}</td>
                                    <td>{appointment.status}</td>
                                    <td>
                                        <button onClick={handleCancel} value={appointment.id} className="btn btn-danger">Cancel</button>
                                        <button onClick={handleFinish} value={appointment.id} className="btn btn-success">Finish</button>
                                    </td>
                                </tr>
                            );
                        }
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default AppointmentList2;
