import React, { useState } from "react";

function AddManufacturer() {
  const [name, setManufacturer] = useState("");

  async function handleSubmit(event) {
    event.preventDefault();
    const data = {
      name,
    };

    const addManufacturerUrl = "http://localhost:8100/api/manufacturers/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(addManufacturerUrl, fetchConfig);
    if (response.ok) {
      const AddManufacturer = await response.json();
      setManufacturer("");
    }
  }

  function handleChangeManufacturer(event) {
    const { value } = event.target;
    setManufacturer(value);
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h2>Create a manufacturer</h2>
          <form onSubmit={handleSubmit} id="add-manufacturer-form">
            <div className="form-floating mb-3">
              <input
                value={name}
                onChange={handleChangeManufacturer}
                placeholder="manufacturer_name"
                required
                type="text"
                name="manufacturer_name"
                id="manufacturer_name"
                className="form-control"
              />
              <label htmlFor="manufacturer_name">Manufacturer name</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default AddManufacturer;
