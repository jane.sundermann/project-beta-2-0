import React, { useEffect, useState } from "react";

function SoldStatus({ soldStatus }) {
  let sold = soldStatus;
  let itemContent = "Sold";
  if (!sold) {
    itemContent = "Unsold";
  }

  return <td className="item">{itemContent}</td>;
}

function ListAutomobiles() {
  const [automobiles, setAutomobiles] = useState([]);

  async function fetchAutomobiles() {
    const automobilesUrl = "http://localhost:8100/api/automobiles/";
    const response = await fetch(automobilesUrl);

    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.autos);
    } else {
      alert("Error fetching automobile data");
    }
  }

  useEffect(() => {
    fetchAutomobiles();
  }, []);

  return (
    <>
      <h1>Automobiles</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Color</th>
            <th>Year</th>
            <th>Model</th>
            <th>Manufacturer</th>
            <th>Sold</th>
          </tr>
        </thead>
        <tbody>
          {automobiles?.map((autos) => {
            return (
              <tr key={autos.href}>
                <td>{autos.vin}</td>
                <td>{autos.color}</td>
                <td>{autos.year}</td>
                <td>{autos.model.name}</td>
                <td>{autos.model.manufacturer.name}</td>
                <SoldStatus soldStatus={autos.sold} />
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default ListAutomobiles;
