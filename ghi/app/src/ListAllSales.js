import React, { useEffect, useState } from "react";

function ListAllSales() {
  const [sales, setSales] = useState([]);

  async function fetchSales() {
    const salesUrl = "http://localhost:8090/api/sales/";
    const response = await fetch(salesUrl);

    if (response.ok) {
      const data = await response.json();
      setSales(data.sales);
    } else {
      alert("Error fetching sales data");
    }
  }

  useEffect(() => {
    fetchSales();
  }, []);

  return (
    <>
      <h1>Sales</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson Employee ID</th>
            <th>Salesperson Name</th>
            <th>Customer</th>
            <th>Vin</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales?.map((sale) => {
            return (
              <tr key={sale.id}>
                <td>{sale.salesperson.employee_id}</td>
                <td>
                  {sale.salesperson.first_name} {sale.salesperson.last_name}{" "}
                </td>
                <td>
                  {sale.customer.first_name} {sale.customer.last_name}
                </td>
                <td>{sale.automobile.vin}</td>
                <td>{sale.price}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default ListAllSales;
