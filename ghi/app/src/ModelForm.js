import { useState, useEffect } from "react";
import './index.css';

function ModelForm() {
    const [ manufacturers, setManufacturers ] = useState([]);
    const [ modelName, setModelName ] = useState('');
    const [ pictureUrl, setPictureUrl ] = useState('');
    const [ manufacturer, setManufacturer ] = useState('');
    // Added a state for error message below
    const [errorMessage, setErrorMessage] = useState('');


    const handleModelNameChange = (e) => {
        const value = e.target.value;
        setModelName(value);
    }

    const handlePictureUrlChange = (e) => {
        const value = e.target.value;
        setPictureUrl(value);
    }

    const handleManufacturerChange = (e) => {
        const value = e.target.value;
        setManufacturer(value);
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';
      try {
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
      } catch (err) {
        console.error(err);
      }
    }

    const handleSubmit =  async (e) => {
        e.preventDefault();

        // Added validation to check if picture url is too long
        if (pictureUrl.length > 255) {
          setErrorMessage('The picture URL is too long. Please use a shorter URL.');
          return;
        }

        const url = 'http://localhost:8100/api/models/';

        const data = {};
        data.name = modelName;
        data.picture_url = pictureUrl;
        data.manufacturer_id = manufacturer;
        const fetchOptions = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(url, fetchOptions);
        if (response.ok) {
            const newModel = await response.json();
            setModelName('');
            setPictureUrl('');
            setManufacturer('');
            setErrorMessage(''); // Clear error message on success
        } else {
          console.error(response.status);
          console.log("hello");
        }

    }

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a vehicle model</h1>
            <form onSubmit={handleSubmit} id="create-model-form">
              <div className="form-floating mb-3">
                <input onChange={handleModelNameChange} placeholder="model_name" required value={modelName} type="text" name="model_name" id="model_name" className="form-control"/>
                <label htmlFor="model_name">Model name...</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePictureUrlChange} placeholder="picture_url" required type="url" value={pictureUrl} name="picture_url" id="picture_url" className="form-control"/>
                <label htmlFor="picture_url">Picture URL...</label>
              </div>
              {/* Added code for errorMessage functionality */}
              {errorMessage && (
                <div className="alert alert-danger" role="alert">
                  {errorMessage}
                </div>
              )}
            <div className="margin-bottom">
              <select onChange={handleManufacturerChange} required name="manufacturer" id="manufacturer" value={manufacturer} className="form-select">
                  <option value="">Choose a manufacturer...</option>
                  {manufacturers.map(manufacturer => {
                    return (
                    <option key={manufacturer.href} value={manufacturer.id}>{manufacturer.name}</option>
                    );
                  })}
              </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}


export default ModelForm;