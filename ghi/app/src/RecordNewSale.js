import React, { useEffect, useState } from "react";

function RecordNewSale() {
  const [automobile_vins, setAutomobileVins] = useState([]);
  const [automobile_vin, setAutomobileVin] = useState("");
  const [salesperson, setSalesPerson] = useState("");
  const [salespersons, setSalesPersons] = useState([]);
  const [customer, setCustomer] = useState("");
  const [customers, setCustomers] = useState([]);
  const [price, setPrice] = useState("");

async function handleSubmit(event){
  event.preventDefault();

  const data = {};
  data.automobile = automobile_vin;
  data.salesperson = salesperson;
  data.customer = customer;
  data.price = price;


  const salerecordUrl = "http://localhost:8090/api/sales/"
  const fetchConfig = {
    method:"POST",
    body:JSON.stringify(data),
    headers: {
      "Content-Type": "application/json",
  },
  };

  const response = await fetch(salerecordUrl, fetchConfig);
  if (response.ok) {
    const newSaleRecord = await response.json();
    setAutomobileVin("")
    setSalesPerson("")
    setCustomer("")
    setPrice("")

    const automobile_vin = data.automobile
    const sold_automobile = `http://localhost:8100${automobile_vin}`
    const soldConfig = {
      method:"PUT",
      body: JSON.stringify({ "sold": true }),
      headers: {
        "Content-Type": "application/json",
    },
  }
  const soldResponse = await fetch(sold_automobile, soldConfig)
}}

  async function fetchData() {
    const auto_vinUrl = "http://localhost:8100/api/automobiles/";
    const response = await fetch(auto_vinUrl);

    if (response.ok) {
      const data = await response.json();
      setAutomobileVins(data.autos);
    } else {
      alert("Error fetching data");
    }
  }

  async function fetchSalesPeople(){
    const salepersonUrl = "http://localhost:8090/api/salespeople/";
    const response = await fetch(salepersonUrl);

    if (response.ok) {
      const data = await response.json();
      setSalesPersons(data.sales_person);
    } else {
      alert("Error fetching salesperson data");
    }
  }

  async function fetchCustomers(){
    const customerUrl = "http://localhost:8090/api/customers/";
    const response = await fetch(customerUrl);

    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    } else {
      alert("Error fetching customer data");
    }
  }

  useEffect(() => {
    fetchData();
    fetchSalesPeople();
    fetchCustomers();
  }, []);

  const handleAutomobileChange = (event) => {
    const value = event.target.value;
    setAutomobileVin(value);
  }
  const handleSalepeopleChange = (event) => {
    const value = event.target.value;
    setSalesPerson(value);
  }
  const handleCustomerChange = (event) => {
    const value = event.target.value;
    setCustomer(value);
  }
  const handlePriceChange = (event) => {
    const value = event.target.value;
    setPrice(value);
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Record a new sale</h1>
          <form onSubmit={handleSubmit} id="sale-record-form">
            <div className="mb-3">
              <select
               onChange={handleAutomobileChange}
               value={automobile_vin}
                required
                name="automobile_vin"
                id="automobile_vin"
                className="form-select"
              >
                <option value="">Automobile VIN</option>
                {automobile_vins?.map((vin) => {
                  return (
                    <option key={vin.id} value={vin.href}>
                      {vin.vin}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="mb-3">
              <select
              onChange={handleSalepeopleChange}
              value={salesperson}
                required
                name="sales_person"
                id="sales_person"
                className="form-select"
              >
                <option value="">Salesperson</option>
                {salespersons?.map((sales_person) => {
                  return (
                    <option key={sales_person.id} value={sales_person.id}>
                      {sales_person.first_name} {sales_person.last_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="mb-3">
              <select
               onChange={handleCustomerChange}
               value={customer}
                required
                name="customers"
                id="customers"
                className="form-select"
              >
                <option value="">Customer</option>
                {customers?.map((customers) => {
                  return (
                    <option key={customers.id} value={customers.id}>
                      {customers.first_name} {customers.last_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input
               onChange={handlePriceChange}
               value={price}
                placeholder="price"
                required
                type="text"
                name="price"
                id="price"
                className="form-control"
              />
              <label htmlFor="price">Price</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default RecordNewSale;
