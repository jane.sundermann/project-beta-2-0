from django.urls import path
from .views import (
    list_salespeople,
    delete_salesperson,
    add_customer,
    delete_customer,
    sold,
    delete_sale,
)


urlpatterns = [
    path('salespeople/', list_salespeople, name="list_salespeople"),
    path('salespeople/<int:id>/', delete_salesperson, name="delete_salespeople"),
    path('customers/', add_customer, name="add_customer"),
    path('customers/<int:id>/', delete_customer, name="delete_customer"),
    path('sales/', sold, name="sold"),
    path('sales/<int:id>', delete_sale, name="delete_sale"),
]
