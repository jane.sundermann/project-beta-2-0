from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import SalesPerson, Customer, Sale, AutomobileVO
# Create your views here.


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["first_name", "last_name", "employee_id", "id"]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ["first_name", "last_name", "address", "phone_number", "id"]


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "sold", "href"]


class RecordSaleEncoder(ModelEncoder):
    model = Sale
    properties = ["automobile", "salesperson", "customer", "price", "id"]
    encoders = {
        "salesperson": SalesPersonEncoder(),
        "customer": CustomerEncoder(),
        "automobile": AutomobileVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def list_salespeople(request):
    if request.method == "GET":
        sales_person = SalesPerson.objects.all()

        return JsonResponse(
            {"sales_person": sales_person},
            encoder=SalesPersonEncoder,
        )

    else:
        content = json.loads(request.body)
        salespeople = SalesPerson.objects.create(**content)

    return JsonResponse(
        salespeople,
        encoder=SalesPersonEncoder,
        safe=False,
    )


@require_http_methods(["DELETE"])
def delete_salesperson(request, id):
    SalesPerson.objects.filter(id=id).delete()
    return JsonResponse(
            {"message": "Salesperson Deleted"},
            status=400,
        )


@require_http_methods(["GET", "POST"])
def add_customer(request):
    if request.method == "GET":
        customers = Customer.objects.all()

        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )

    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)

    return JsonResponse(
        customer,
        encoder=CustomerEncoder,
        safe=False,
    )


@require_http_methods(["DELETE"])
def delete_customer(request, id):
    Customer.objects.filter(id=id).delete()
    return JsonResponse(
            {"message": "Customer Deleted"},
            status=400,
        )


@require_http_methods(["GET", "POST"])
def sold(request):
    if request.method == "GET":
        sales = Sale.objects.all()

        return JsonResponse(
            {"sales": sales},
            encoder=RecordSaleEncoder,
            safe=False,
        )

    else:
        content = json.loads(request.body)
        automobile = content["automobile"]

        try:
            content["salesperson"] = SalesPerson.objects.get(id=content.get("salesperson"))
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Salesperson Not Found"},
                status=400
            )

        try:
            content["automobile"] = AutomobileVO.objects.get(href=content.get("automobile"))
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Automobile Not Found"},
                status=400
            )

        try:
            content["customer"] = Customer.objects.get(id=content.get("customer"))
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer Not Found"},
                status=400
            )

        sale = Sale.objects.create(**content)

        AutomobileVO.objects.filter(vin=automobile).update(sold=False)

    return JsonResponse(
        sale,
        encoder=RecordSaleEncoder,
        safe=False,
    )


@require_http_methods(["DELETE"])
def delete_sale(request, id):
    Sale.objects.filter(id=id).delete()
    return JsonResponse(
            {"message": "Sale Removed"},
            status=400,
        )
