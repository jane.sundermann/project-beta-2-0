from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Appointment, AutomobileVO, Technician

# Create your views here.
class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id",
        "vin",
        "sold",
        "href",
        ]

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date_time",
        "reason",
        "status",
        "vin",
        "vip",
        "customer",
        "technician",
    ]

    encoders={
        "technician": TechnicianEncoder(),
    }

@require_http_methods(["DELETE", "GET", "POST"])
def technician(request, technician_id=None):
    if request.method == "GET" and technician_id==None:
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
            safe=False,
        )
    elif request.method == "GET" and technician_id != None:
        technician = Technician.objects.get(id=technician_id)
        return JsonResponse(
            technician,
            TechnicianEncoder,
            safe=False
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            TechnicianEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Technician.objects.filter(id=technician_id).delete()
        return JsonResponse(
            {"message": count > 0},
        )

@require_http_methods(["DELETE", "GET", "POST"])
def appointments(request, appointment_id=None):
    if request.method == "GET" and appointment_id==None:
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
            safe=False
        )
    elif request.method == "GET" and appointment_id != None:
        appointment = Appointment.objects.get(id=appointment_id)
        return JsonResponse(
            appointment,
            AppointmentEncoder,
            safe=False
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(id=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            print("That's not a valid ID for technician")
        content["status"] = "created"
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            AppointmentEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=appointment_id).delete()
        return JsonResponse({"deleted": count > 0})

@require_http_methods(["PUT"])
def canceled(request, appointment_id):
    Appointment.objects.filter(id=appointment_id).update(status="canceled")
    appointment = Appointment.objects.get(id=appointment_id)
    return JsonResponse(
        appointment,
        AppointmentEncoder,
        safe=False,
    )

@require_http_methods(["PUT"])
def finished(request, appointment_id):
    Appointment.objects.filter(id=appointment_id).update(status="finished")
    appointment = Appointment.objects.get(id=appointment_id)
    return JsonResponse(
        appointment,
        AppointmentEncoder,
        safe=False,
    )