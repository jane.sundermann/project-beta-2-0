from django.db import models
from django.urls import reverse

# Create your models here.
class AutomobileVO(models.Model):
    # "unique=True" below ensures each record in AutomobileVO table has distinct "href" and "vin" field
    href = models.CharField(max_length=100, unique=True, blank=True)
    sold = models.BooleanField(default=False)
    vin = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.vin

class Technician(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.CharField(max_length=50)

    def __str__(self):
        return self.first_name + ' ' + self.last_name

    # def get_api_url(self):
    #     return reverse("technician", kwargs={"id": self.technician_id})

class Appointment(models.Model):
    date_time = models.DateTimeField(null=True)
    reason = models.TextField(null=True)
    status = models.CharField(max_length=100, null=True)
    vin = models.CharField(max_length=100, unique=True, null=True)
    vip = models.BooleanField(default=False)
    customer = models.CharField(max_length=100, null=True)

    technician = models.ForeignKey(
        Technician,
        related_name="technician",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.vin

    # # This method uses the 'reverse' function to create the URL for the API endpoint that
    # # corresponds to the 'Appointment' model instance. "Api...show" etc. should be the name of the
    # # URL pattern that shows a single appointment and "kwargs..." passes the id of
    # # the appointment instance to the URL pattern to generate the correct URL
    # def get_api_url(self):
    #     return reverse("appointments", kwargs={"id": self.appointment_id})

    # "Your vin field should be of type CharField (It should not be an AutomobileVO foreign key)"