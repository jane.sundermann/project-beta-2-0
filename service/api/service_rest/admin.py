from django.contrib import admin
from .models import Appointment, Technician, AutomobileVO

# Register your models here.
@admin.register(Appointment)
class Appointmemnt(admin.ModelAdmin):
    pass

@admin.register(AutomobileVO)
class AutomobileVO(admin.ModelAdmin):
    pass

@admin.register(Technician)
class Technician(admin.ModelAdmin):
    pass


