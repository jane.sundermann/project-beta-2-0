# ProjectBeta


---

## How to Run this App

Make sure that you have Docker, Git, and Node .js 18.2 or above.

Fork this repository in GitLab.

Clone the forked repository onto your local computer. To do this, run
the following command in Terminal:

	git clone https://gitlab.com/jane.sundermann/project-beta.git

Build and run the project using Docker by running these commands in Terminal:

	- docker volume create beta-data
	- docker compose build
	- docker compose up

Make sure all of your Docker containers are up and running.

View and interact with the application in the browser at http://localhost:3000/

---

## API Documentation

All API endpoints for the Sales microservice can be used to send and view data through Insomnia and your browser.

---

## Diagram

<img src="Project_Beta_Diagram.jpg">

---

Diagram synopsis:

A user can create a new model by going to the URL that displays the "Add model" components, fill out the form, and send the form back to the API end point with a POST request. Name and URL are input text fields, the manufacturer is selected from a drop down. The manufacturers are displayed in the drop down by a separate API end point call to manufacturers in our inventory app. Once a form is successfully sent back to the API end point it is displayed in the "List all models" component.

To access objects from a different project or an "external application" the poller creates a reference, represented by VO after the model name (for example AutomobileVO). This stores the import_href, VIN and sold status to allow our applications to interact with one another.

---

## Inventory


### Manufacturers

---

| Action                         | Method | URL                                          |
| ------------------------------ | ------ | -------------------------------------------- |
| List manufacturers             | GET    | http://localhost:8100/api/manufacturers/     |
| Create a manufacturer          | POST   | http://localhost:8100/api/manufacturers/     |
| Get a specific manufacturer    | GET    | http://localhost:8100/api/manufacturers/:id/ |
| Update a specific manufacturer | PUT    | http://localhost:8100/api/manufacturers/:id/ |
| Delete a specific manufacturer | DELETE | http://localhost:8100/api/manufacturers/:id/ |

The delete endpoint will throw a 400 status code if the manufacturer does not exist.

---

### Create Manufacturer
---

To create and update a manufacturer, only the manufacturer's name is a required field. The output will return the following:

---

```
{
  "name": "Chrysler"
}
```

---

Creating, getting, and updating a single manufacturer returns its name, href, and id. The output will return the following:

---

```
{
  "href": "/api/manufacturers/1/",
  "id": 1,
  "name": "Chrysler"
}
```

---

The list of manufacturers is a dictionary with the key "manufacturers" set to a list of manufacturers. The output will return the following:

---

```
{
  "manufacturers": [
    {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  ]
}
```

---

### Automobile Information

---

| Action                       | Method | URL                                         |
| ---------------------------- | ------ | ------------------------------------------- |
| List automobiles             | GET    | http://localhost:8100/api/automobiles/      |
| Create an automobile         | POST   | http://localhost:8100/api/automobiles/      |
| Get a specific automobile    | GET    | http://localhost:8100/api/automobiles/:vin/ |
| Update a specific automobile | PUT    | http://localhost:8100/api/automobiles/:vin/ |
| Delete a specific automobile | DELETE | http://localhost:8100/api/automobiles/:vin/ |

The delete endpoint will throw a 400 status code if the automobile does not exist.

**Note:** The identifiers for automobiles in this API are **not** integer ids. They are the VIN for the specific automobile.

---

### Create Automobile
---

Create an automobile with its color, year, VIN, and the id of the vehicle model. The output will return the following:

---

```
{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}
```

---

### Specific Automobile

---

You would query an automobile by its VIN. Example URL:

http://localhost:8100/api/automobiles/1C3CC5FB2AN120174/

to get the details for the car with the VIN "1C3CC5FB2AN120174". The details for an automobile include its model and manufacturer. The output will return the following:

---

```
{
  "href": "/api/automobiles/1C3CC5FB2AN120174/",
  "id": 1,
  "color": "yellow",
  "year": 2013,
  "vin": "1C3CC5FB2AN120174",
  "model": {
    "href": "/api/models/1/",
    "id": 1,
    "name": "Sebring",
    "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
    "manufacturer": {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  },
  "sold": false
}
```

---

### Update Automobile

---

Update the color, year, and sold status of an automobile. Use the following format:

---

```
{
  "color": "red",
  "year": 2012,
  "sold": true
}
```

---

### List Automobiles

---

Get a list of automobiles that returns a dictionary with the key "autos" set to a list of automobile information. The output will return the following:

---

```
{
  "autos": [
    {
      "href": "/api/automobiles/1C3CC5FB2AN120174/",
      "id": 1,
      "color": "yellow",
      "year": 2013,
      "vin": "1C3CC5FB2AN120174",
      "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "Sebring",
        "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
        "manufacturer": {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "Daimler-Chrysler"
        }
      },
      "sold": false
    }
  ]
}
```


### Vehicle Models

| Action                          | Method | URL                                  |
| ------------------------------- | ------ | ------------------------------------ |
| List vehicle models             | GET    | http://localhost:8100/api/models/    |
| Create a vehicle model          | POST   | http://localhost:8100/api/models/    |
| Get a specific vehicle model    | GET    | http://localhost:8100/api/models/id/ |
| Update a specific vehicle model | PUT    | http://localhost:8100/api/models/id/ |
| Delete a specific vehicle model | DELETE | http://localhost:8100/api/models/id/ |

Return value expected for list of vehicle models (GET request above):

---

```
{
	"models": [
		{
			"href": "/api/models/1/",
			"id": 1,
			"name": "Sebring",
			"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
			"manufacturer": {
				"href": "/api/manufacturers/1/",
				"id": 1,
				"name": "Chrysler"
			}
		},
		{
			"href": "/api/models/2/",
			"id": 2,
			"name": "Outback",
			"picture_url": "https://s7d1.scene7.com/is/image/scom/RDB_default_pass_scaled?$900p$",
			"manufacturer": {
				"href": "/api/manufacturers/2/",
				"id": 2,
				"name": "Subaru"
			}
		},
	]
}
```

In order to create or update a vehicle model, send this JSON body:

```
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 1
}
```

Updating a vehicle model can accept the name and/or picture URL. An important note on the picture URL: the URL must be 200 characters or less in order to
successfully submit when updating or creating a vehicle model.

```
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg"
}
```

Return value of creating or updating a vehicle model:

- This will return the info on the manufacturer too.

```
{
	"href": "/api/models/1/",
	"id": 1,
	"name": "Sebring",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Chrysler"
	}
}
```

---
# Service microservice

## Models and Value Objects:

The Service microservice has 3 models: Technician, AutomobileVO, and Appointment.

1. The Technician model contains fields for the first and last name of the employee as well as the employee ID. This model keeps track of all technicians.
2. The AutomobileVO model contains fields that indicate a vin for an automobile and whether or not the automobile was sold. The AutomobileVO is a value object that brings in data about automobiles in the inventory through use of a poller. The service poller updates AutomobileVO every 60 seconds with updated VINs from the inventory service.
3. The Appointment model contains fields for the date-tiem of appointments, reason for appointment, status of the appointment (created, canceled, or finished), customer, and technician assigned to the appointment. This keeps track of
   all service appointents for all automobiles.The Appointment model interacts with the Technician model with the technician field as its foreign key. Additionally, the Appointment model has a "vip" field that returns "True" if the vin attached to an appointment matches a vin on an automobile that is marked as something we sold on the AutomobileVO. This lets us know that for that appointment, the customer should receive all of our vip perks.

## Accessing Endpoints to Send and View Data: Access through Insomnia

- All API endpoints for the Service microservice can be used to send and view data through Insomnia and your browser.

---

### Technicians

| Action                     | Method | URL                                       |
| -------------------------- | ------ | ----------------------------------------- |
| List technicians           | GET    | http://localhost:8080/api/technicians/    |
| Create a technician        | POST   | http://localhost:8080/api/technicians/    |
| Get specific technician    | GET    | http://localhost:8080/api/technicians/id/ |
| Delete specific technician | DELETE | http://localhost:8080/api/technicians/id/ |

---

### LIST TECHNICIANS:

To view a list of all the technicians that we have currently employed, send a GET request to
http://localhost:8080/api/technicians/ within Insomnia. Since it is a GET request, you do not need to pass
in any data with the request. The return value of the request should look like this in Insomnia,
with a full list of the technicians, their first and last names, as well as their automatically generated
ids (i.e. technician_id) and employee ids.

```
{
	"technicians": [
		{
			"id": 1,
			"first_name": "Jane",
			"last_name": "Sundermann",
			"employee_id": "test1234"
		},
		{
			"id": 6,
			"first_name": "Pat",
			"last_name": "Kelly",
			"employee_id": "6666"
		},
		{
			"id": 7,
			"first_name": "Yongwoo Dave",
			"last_name": "Seo",
			"employee_id": "7777"
		},
    ]
}
```

---

### CREATE TECHNICIAN:

Let's say that we just hired a new technician. To create a technician, send a POST request to
the URL http://localhost:8080/api/technicians/ within Insomnia. The data that you will need to pass in
should be in JSON and follow this specific format:

```
{
		"first_name": "Evy",
		"last_name": "Bickel",
		"employee_id": "8888888"
}
```

Upon creating the new technician, you should see the following return value of the POST request within Insomnia
(below). The id for the technician is automatically generated upon creation of the technician.

```
{
	"id": 22,
	"first_name": "Evy",
	"last_name": "Bickel",
	"employee_id": "8888888"
}
```

To confirm that the technician has been created, send another GET request in Insomnia to http://localhost:8080/api/technicians/
to view the list of technicians. The technician that you created should now appear within that list.

---

### GET SPECIFIC TECHNICIAN:

To view the detail of a particular technician, look at the list of technicians (see GET request above)
and retrieve the id (an integer) of the technician for whom you want to view the detail.
Place that technician id (e.g., "1") in the id field of http://localhost:8080/api/technicians/id/ and send a
GET request to that address within Insomnia. (For instance, if you want to view the detail of a specific
technician with the id of 1, you would send a GET request to the following address: http://localhost:8080/api/technicians/1/
in Insomnia). The return value of the GET request will look like this:

```
{
	"id": 1,
	"first_name": "Jane",
	"last_name": "Sundermann",
	"employee_id": "test1234"
}
```

---

### DELETE TECHNICIAN:

If a technician has resigned or been fired, you can delete the technician in Insomnia. View the list of technicians
(see GET request above) in Insomnia, and retrieve the id (an integer) of the technician that you want to delete.
Place that technician id (e.g., "1") in the id field of http://localhost:8080/api/technicians/id/ and send a
DELETE request to that address within Insomnia. (For instance, if you want to delete a technician with the id of 1,
you would send a DELETE request to the following address: http://localhost:8080/api/technicians/1/
in Insomnia). The return value of the DELETE request will look like this, with "true" meaning that the technician
has been successfully deleted:

```
{
	"message": true
}
```

To confirm that the technician has been deleted, send another GET request in Insomnia to http://localhost:8080/api/technicians/
to view the list of technicians. The technician that you deleted should no longer be populated within that list.

---

### Appointments

| Action                      | Method | URL                                               |
| --------------------------- | ------ | ------------------------------------------------- |
| List service appointments   | GET    | http://localhost:8080/api/appointments/           |
| Get a specific appointment  | GET    | http://localhost:8080/api/appointments/id/        |
| Create service appointment  | POST   | http://localhost:8080/api/appointments/           |
| Delete specific appointment | DELETE | http://localhost:8080/api/appointments/id/        |
| Cancel specific appointment | DELETE | http://localhost:8080/api/appointments/id/cancel/ |
| Finish specific appointment | DELETE | http://localhost:8080/api/appointments/id/finish/ |

---

### LIST APPOINTMENTS:

To view a list of all the appointments in the system, send a GET request to
http://localhost:8080/api/appointments/ within Insomnia. Since it is a GET request, you do not need to pass
in any data with the request. The return value of the request should look like this in Insomnia (below)
with a full list of the appointments and their details.

```
{
	"appointments": [
		{
			"id": 13,
			"date_time": "2026-03-27T00:00:00+00:00",
			"reason": "Oil change",
			"status": "canceled",
			"vin": "2C3CDXBGXDH581047",
			"vip": false,
			"customer": "Hermione Granger",
			"technician": {
				"id": 8,
				"first_name": "Henry",
				"last_name": "Sundermann",
				"employee_id": "8888"
			}
		},
		{
			"id": 26,
			"date_time": "2026-12-31T00:00:00+00:00",
			"reason": "Fix oil leak",
			"status": "finished",
			"vin": "JF2SJAACXEH465531",
			"vip": true,
			"customer": "Draco Malfow",
			"technician": {
				"id": 1,
				"first_name": "Jane",
				"last_name": "Sundermann",
				"employee_id": "test1234"
			}
		},
	]
}
```

---

### CREATE APPOINTMENT:

Let's say that we need to create a new appointment. To create a appointment, send a POST request to
the URL http://localhost:8080/api/appointments/ within Insomnia. The data that you will need to pass in
should be in JSON and follow this specific format:

```
{
	"date_time": "2028-3-30",
	"reason": "Repair tires",
	"vin": "2HGFA168X8H364512",
	"vip": false,
  "customer": "Hermione Granger",
	"technician": 1
}
```

Upon creation of the appointment, you should see the following return value of the POST request within Insomnia
(below). The id for the appointment is automatically generated upon creation of the appointment.

```
{
	"id": 38,
	"date_time": "2028-3-30",
	"reason": "Repair tires",
	"status": "created",
	"vin": "2HGFA168X8H364512",
	"vip": false,
	"customer": "Hermione Granger",
	"technician": {
		"id": 1,
		"first_name": "Jane",
		"last_name": "Sundermann",
		"employee_id": "test1234"
	}
}
```

To confirm that the appointment has been created, send another GET request in Insomnia to http://localhost:8080/api/appointments/
to view the list of appointments. The appointment that you created should now appear within that list.

---

### GET SPECIFIC APPOINTMENT:

To view the detail of a particular appointment, look at the list of appointments (see GET request above)
and retrieve the id (an integer) of the appointment for which you want to view the detail.
Place that appointment id (e.g., "1") in the id field of http://localhost:8080/api/appointments/id/ and send a
GET request to that address within Insomnia. (For instance, if you want to view the detail of a specific
appointment with the id of 1, you would send a GET request to the following address: http://localhost:8080/api/appointments/1/
in Insomnia). The return value of the GET request will look like this:

```
{
	"id": 32,
	"date_time": "2025-01-15T09:28:00+00:00",
	"reason": "oil change",
	"status": "finished",
	"vin": "1G1JC52F037182555",
	"vip": false,
	"customer": "Luna Lovegood",
	"technician": {
		"id": 9,
		"first_name": "Loki",
		"last_name": "Sundermann",
		"employee_id": "9999"
	}
}
```

---

### DELETE APPOINTMENT:

If you need to delete an appointment, you can delete the appointment in Insomnia. View the list of appointments
(see GET request above) in Insomnia, and retrieve the id (an integer) of the appointment that you want to delete.
Place that appointment id (e.g., "1") in the id field of http://localhost:8080/api/appointments/id/ and send a
DELETE request to that address within Insomnia. (For instance, if you want to delete a appointment with the id of 1,
you would send a DELETE request to the following address: http://localhost:8080/api/appointments/1/
in Insomnia). The return value of the DELETE request will look like this, with "true" meaning that the appointment
has been successfully deleted:

```
{
	"deleted": true
}
```

To confirm that the appointment has been deleted, send another GET request in Insomnia to http://localhost:8080/api/appointments/
to view the list of appointments. The appointment that you deleted should no longer be populated within that list.

---

### CANCEL APPOINTMENT:

If you need to cancel an appointment, you can change the appointment's status field to "canceled" in Insomnia.
To cancel a specific appointment, first view the list of appointments (see GET request above) in Insomnia, and
retrieve the id (an integer) of the appointment that you want to cancel. Place that appointment id (e.g., "1") in
the id field of http://localhost:8080/api/appointments/id/cancel/ and send a PUT request to that address within Insomnia.
(For instance, if you want to cancel an appointment with the id of 1, you would send a PUT request to the following address:
http://localhost:8080/api/appointments/1/cancel/ in Insomnia). As part of the PUT request, you must pass in JSON data, and
to mark the appointment as canceled, the JSON that you pass in with the request should look just like this:

```
{
	"status": "Canceled"
}
```

The return value of the PUT request will look like this (below). As you can see, the status of the appointment is now marked
as "canceled".

```
{
	"id": 35,
	"date_time": "2024-01-05T09:35:00+00:00",
	"reason": "oil change",
	"status": "canceled",
	"vin": "JTHBK1GG8E2152226",
	"vip": false,
	"customer": "Voldemort",
	"technician": {
		"id": 12,
		"first_name": "Yuri",
		"last_name": "Seo",
		"employee_id": "33333"
	}
}
```

To confirm that the appointment has only been marked as "canceled" and has NOT been deleted from the system, send another GET request
in Insomnia to http://localhost:8080/api/appointments/ to view the list of appointments. The appointment that you canceled should
still exist in the list, just now with the status of "canceled".

---

### FINISH APPOINTMENT:

If you need to mark an appointment as "finished" (i.e. complete), you can change the appointment's status field to "finished" in Insomnia.
To "finish" a specific appointment, first view the list of appointments (see GET request above) in Insomnia, and
retrieve the id (an integer) of the appointment that you want to finish. Place that appointment id (e.g., "1") in
the id field of http://localhost:8080/api/appointments/id/finish/ and send a PUT request to that address within Insomnia.
(For instance, if you want to finish an appointment with the id of 1, you would send a PUT request to the following address:
http://localhost:8080/api/appointments/1/finish/ in Insomnia). As part of the PUT request, you must pass in JSON data, and
to mark the appointment as finished, the JSON that you pass in with the request should look just like this:

```
{
	"status": "finished"
}
```

The return value of the PUT request will look like this (below). As you can see, the status of the appointment is now marked
as "canceled".

```
{
	"id": 35,
	"date_time": "2024-01-05T09:35:00+00:00",
	"reason": "oil change",
	"status": "finished",
	"vin": "JTHBK1GG8E2152226",
	"vip": false,
	"customer": "Voldemort",
	"technician": {
		"id": 12,
		"first_name": "Yuri",
		"last_name": "Seo",
		"employee_id": "33333"
	}
}
```

To confirm that the appointment has only been marked as "finished" and has NOT been deleted from the system, send another GET request
in Insomnia to http://localhost:8080/api/appointments/ to view the list of appointments. The appointment that you finished should
still exist in the list, just now with the status of "finished".

---

# Sales Microservice

## Models and Value Objects

The Sales microservice has 4 models:

- Salesperson
- Customer
- Sale
- AutomobileVO

The Salesperson model keeps track of all salespeople data. Salesperson contains these fields:

- first_name
- last_name
- employee_id

The Customer model keeps track of all customer data. Customer contains these fields:

- first_name
- last_name
- address
- phone_number

The Sale model stores data from an automobile purchase. Each field is a foreign key field **except** for price. Sale contains these fields:

- automobile
- salesperson
- customer
- price

The AutomobileVO model stores a reference to automobile in the inventory app. The poller updates every 60 seconds with new AutomobileVO VINs and contains these fields:

- vin
- sold

---

### Salespeople

| Action                        | Method | URL                                        |
| ----------------------------- | ------ | ------------------------------------------ |
| List salespeople              | GET    | http://localhost:8090/api/salespeople/     |
| Create a salesperson          | POST   | http://localhost:8090/api/salespeople/     |
| Delete a specific salesperson | DELETE | http://localhost:8090/api/salespeople/:id/ |

The delete endpoint will throw a 400 status code if salespeople ID does not exist.

---

### Create a Salesperson

To create a salesperson through this API, use the following format to input the data and submit as a POST request:

```
{
"first_name": "Pugs",
"last_name": "Peeps",
"employee_id": 1
}
```

Upon creating the new salesperson, the following value will return:

```
{
"id": 1,
"first_name": "Pugs",
"last_name": "Peeps",
"employee_id": 1
}
```

---

### List Salepeople

Sending a request to this end point will give you a list of all the salepeople.

Data is not needed to send the request given that it is a GET request. The expected return value of the request will look like this:

```
{
"sales_person": [
		{
			"first_name": "Pugs",
			"last_name": "Peeps",
			"employee_id": "1",
			"id": 1
		},
        {
		    "first_name": "Refill",
			"last_name": "Coffee",
			"employee_id": "2",
			"id": 2
		}
    ]
}
```

---

### Customers

---

| Action                     | Method | URL                                      |
| -------------------------- | ------ | ---------------------------------------- |
| List customers             | GET    | http://localhost:8090/api/customers/     |
| Create a customer          | POST   | http://localhost:8090/api/customers/     |
| Delete a specific customer | DELETE | http://localhost:8090/api/customers/:id/ |

The delete endpoint will throw a 400 status code if the customer ID does not exist.

---

### Create a Customer

Sending a POST request to this end point with the required fields will create a new customer. The return value of the POST request will look like this:

```
{
	"first_name": "Tillamook",
	"last_name": "Cheese",
	"address": "1234 Moon St, Seattle, WA 9814",
	"phone_number": "2065678912"
}
```

---

### List Customers

When the list customers end point is sent a GET request, the following format will be displayed, listing all of the customers and their respective data:

```
{
	"customers": [
		{
			"first_name": "Tillamook",
			"last_name": "Cheese",
			"address": "1234 Moon St, Seattle, WA 9814",
			"phone_number": "2065678912",
			"id": 1
		},
        {
			"first_name": "Hello",
			"last_name": "Kitty",
			"address": "5 Apples Lane, London NW93 5VZ",
			"phone_number": "2071234567",
			"id": 2
		}
    ]
}
```

---

### Sales

---

| Action        | Method | URL                                 |
| ------------- | ------ | ----------------------------------- |
| List sales    | GET    | http://localhost:8090/api/sales/    |
| Create a sale | POST   | http://localhost:8090/api/sales/    |
| Delete a sale | DELETE | http://localhost:8090/api/sales/:id |

The delete endpoint will throw a 400 status code if the sale ID does not exist.

---

### Create a Sale

To create a sale, send a POST request to it's end point with the following data in JSON format:

---

```
{
"automobile": "/api/automobiles/4Y1SL65848Z411439/",
"salesperson": "1",
"customer": "1",
"price": 100000
}
```

The return value of the POST request will look like this:

---

```
{
	"automobile": {
		"vin": "4Y1SL65848Z411439",
		"sold": false,
		"href": "/api/automobiles/4Y1SL65848Z411439/"
	},
	"salesperson": {
		"first_name": "Pug",
		"last_name": "Peeps",
		"employee_id": "1",
		"id": 1
	},
	"customer": {
		"first_name": "Tillamook",
		"last_name": "Cheese",
		"address": "1234 Moon St, Seattle, WA 9814",
		"phone_number": "2065678912",
		"id": 1
	},
	"price": 100000,
	"id": 1
}
```

---

To confirm that the sale was created successfully, look at your sales list to verify the sale is there.

---

### List Sales

Sending a GET request to this end point will return a list of all sales. The return value of the GET request will look like this:

```
{
	"sales": [
		{
			"automobile": {
				"vin": "4Y1SL65848Z411439",
				"sold": true,
				"href": "/api/automobiles/4Y1SL65848Z411439/"
			},
			"salesperson": {
				"first_name": "Pug",
				"last_name": "Peeps",
				"employee_id": "1",
				"id": 1
			},
			"customer": {
				"first_name": "Tillamook",
				"last_name": "Cheese",
				"address": "1234 Moon St, Seattle, WA 9814",
				"phone_number": "2065678912",
				"id": 1
			},
			"price": 100000,
			"id": 1
        },
        {
			"automobile": {
            "vin": 7R1VL65848Y411426",
            "sold": true,
            "href": "/api/automobiles/7R1VL65848Y411426/"
			},
			"salesperson": {
		    "first_name": "Refill",
			"last_name": "Coffee",
			"employee_id": "2",
			"id": 2
		},
			"customer": {
			"first_name": "Hello",
			"last_name": "Kitty",
			"address": "5 Apples Lane, London NW93 5VZ",
			"phone_number": "2071234567",
			"id": 2
		},
			"price": 200000,
			"id": 2
		}
    ]
}
```
